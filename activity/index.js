console.log('ES6 Update MiniActivity');


				// 3. create a variable using exponent operator.
const getCube = 2 ** 3;


				// 4. Template Literals
console.log(`The cube 2 is ${getCube}`);


				// 5. Create variable address with array
const address = [258, 'Washington Ave. NW', 'California', 90011];


				// 6. destructure - print - (template literals)
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);


				// 7. animal variableName - Object array 
 const animal = {
 	name: 'Lolong',
 	type: 'Salt Water crocodile',
 	weight: 1075,
 	measurement: '20 ft. 3 in.' 
 }

 				// 8. print out - destructure - template literals
console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}`);


				// 9. create array numbers
let numbers = [1, 2, 3, 4, 5];


				// 10. loop - forEach - arrow function 
numbers.forEach((num) => {
	console.log(num)
})


				// 11. creat var reduceNumber - use reduce method - arrow function 
const reduceNumber = numbers.reduce((x, y) => x + y)
console.log(reduceNumber);

				// 12. Create class of dog 
class Dog{
	details(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
				// 13. create new
const newDog = new Dog();
newDog.name = "Frankie";
newDog.age = 5;
newDog.breed = 'Askal';

console.log(newDog);
