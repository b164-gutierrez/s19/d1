console.log('Hello World')

// ES6 Updates

// 1. Exponent Operator

// syntax
// before (math.pow)
const firstNum = Math.pow(8, 2);

// after
const secondNum = 8 ** 2;

// 2. Template Literals
// allows to write strings w/o using the concatenation operator (+)
// uses backticks (``) ${expression}

const interesRate = .1;
const principal = 1000;

console.log(`The interes is: ${principal * interesRate}`)

// Array Destructuring
// allows us to unpack elements in array into disctinct variables
/*
	syntax:
			let/const [variableNameA, variableNameB, variableNameC,] = array;
*/

const fullName = ['Juan', 'Dela', 'Cruz'];

// pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
		
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! Its nice to meet you.`)

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName[0]} ${middleName[1]} ${lastName[2]}! Its nice to meet you.`)
// helps with code readability

// 4. Object Destructuring
// allows to unpack properties of obejects into distinct variables
/*
	syntax:
			let/const {propertName, propertName, propertName,} = object;
*/
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! Its nice to meet you.`);

// Object Destructuring

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
// Shortens the syntax for accessing properties from object

function getFullName({givenName, maidenName, familyName}){
	console.log(`Hello ${givenName} ${maidenName} ${familyName}! Its nice to meet you.`)
}

getFullName(person);

// 5. Arrow Function

// tradintional functions
function printFullName(firstName, middleName, lastName){
	console.log(`${firstName} ${middleName} ${lastName} `)
}
printFullName('Juan', 'Dela', 'Cruz');


					// Arrow Function
// sample
/*
	syntax:
			let/const variableName = (parameterA, parameterB) => {
				statement
			}
*/


let printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName} `)
}
printFullName1('Jane', 'D', 'Smith');

const students = ['John', 'Jane', 'Jasmine'];

students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// Arrow Function

// for multiple parameter = ((parameter))
students.forEach((student) => {
	console.log(`${student} is a student.`)
});


// fon single parameter = (parameter)
students.forEach(student => {
	console.log(`${student} is a student.`)
});


// Implicit Return Statement
// there are instances when you can omit the "Return" statement

// Pre-Arrow Function

// sample 1 (long method)
// const add = (x, y) =>{
// 	return x + y
// }

// let total = add(1, 2);
// console.log(total);

// sample 2 (short method)
const add = (x, y) => x + y;

let total = add (1, 2);
console.log(total);


// Default Function Argument Value
// Provide a default argument value if none is provided when the function is invoked.

// sample
const greet = (name = 'User') =>{
	return `Goodmorning, ${name}!`
}
console.log(greet('John'));//if no element in (greet('John')) = User


// Class-Base Object Blueprints
// Allows creation of objects using classes as blueprints
// creating a class
	// construction is a special method of a class for creating an object.
	// this keyword that refers to the properties of an object created from the class.

class Car{
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = '2021';

console.log(myCar)


const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);


// Ternary Operator
// (condition) ? truthy : falsy